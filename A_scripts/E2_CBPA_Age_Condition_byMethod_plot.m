%% plot results of mMSE CBPA

restoredefaultpath; clc;
pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
addpath([pn.root, 'T_tools/fieldtrip-20170904']); ft_defaults;

load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat'], 'stat', 'methodLabels')

contrastLabels = {'YA: EC vs. EO'; 'OA: EC vs. EO'; 'EC: OA vs. YA'; 'EO: OA vs. YA'};

% 1 - YA: EC vs. EO
% 2 - OA: EC vs. EO
% 3 - EC: YA vs. OA
% 4 - EO: YA vs. OA

% X, 1... - method in methodLabels

%% FIGURE 5: plot only eyes open age differences: OA > YA

set(0,'DefaultAxesColor',[1 1 1])

indContrast = 4;
labels = {'MSE: Vanilla'; 'MSE: Vanilla + scale-wise bounds'; 'MSE: Lowpass + scale-wise R'; 'MSE: Highpass + scale-wise bounds'; 'MSE: Bandpass + scale-wise bounds';...
    'Bounds: Vanilla'; 'Bounds: Vanilla + scale-wise bounds'; 'Bounds: Lowpass + scale-wise bounds'; 'Bounds: Highpass + scale-wise bounds'; 'Bounds: Bandpass + scale-wise bounds'};

addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/altmany-export_fig-4282d74')

h = figure('units','normalized','position',[0 0 1 .4]);
for indMethod = 1:10
    subplot(2,5,indMethod);
    imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', .5); 
    caxis([-6 6]) 
    hold on;
    imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', stat{indContrast,indMethod}.mask); 
    set(gca, 'XTick', [1:7:42, 42]);
    set(gca, 'XTickLabels', round(stat{1,1}.freq(get(gca, 'XTick')),0));
    set(gca, 'XDir', 'reverse')
    xlabel('Frequency (Hz)'); ylabel('Channel');
    cb = colorbar; set(get(cb,'label'),'string','t values');
    title(labels(indMethod))
end
set(findall(gcf,'-property','FontSize'),'FontSize',15)
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'E3_CBPA_EO_OAvsYA_v2';
% export_fig([pn.plotFolder, figureName, '.pdf'], '-transparent')
% export_fig([pn.plotFolder, figureName, '.eps'], '-transparent')

saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'png');

%% Supplement: plot entropy age difference during eyes open & eyes closed & effects of eye closure (not reported in manuscript)

h = figure('units','normalized','position',[0 0 1 1]);
for indContrast = 1:4
    for indMethod = 1:10
        subplot(10,4,(indMethod-1)*4+indContrast);
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', .5); 
        caxis([-6 6]) 
        hold on;
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', stat{indContrast,indMethod}.mask); 
        set(gca, 'XTick', 1:4:41); set(gca, 'XTickLabel', round(stat{indContrast,indMethod}.freq(1:4:41))); xlabel('Scale [Hz]'); ylabel('Channel');
        title([contrastLabels{indContrast}, ';', methodLabels{indMethod}])
    end
end
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
cBrew = brewermap(500,'RdBu');
cBrew = flipud(cBrew);
colormap(cBrew)
pn.plotFolder = [pn.root, 'C_figures/'];
figureName = 'E3_statsWithMasking_v2';
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');
