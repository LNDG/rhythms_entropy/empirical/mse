%% Plot difference between methods

pn.dataPath = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/C_MSE_Output_v1/';

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/B_data/E_mseMerged.mat', 'mseMerged', 'IDs')

indEO = 2;

figure;
subplot(5,2,1); imagesc(squeeze(nanmean(mseMerged{1,indEO}.MSEVanilla(:,:,:),1))); title('Vanilla');
subplot(5,2,2); imagesc(squeeze(nanmean(mseMerged{1,indEO}.Rlog10Vanilla(:,:,:),1))); title('log10(R)')

subplot(5,2,3); imagesc(squeeze(nanmean(mseMerged{1,indEO}.MSEPointavg(:,:,:),1))); title('Pointavg');
subplot(5,2,4); imagesc(squeeze(nanmean(mseMerged{1,indEO}.Rlog10Pointavg(:,:,:),1))); title('log10(R)')

subplot(5,2,5); imagesc(squeeze(nanmean(mseMerged{1,indEO}.MSEfiltskip(:,:,:),1))); title('Filtskip: LP');
subplot(5,2,6); imagesc(squeeze(nanmean(mseMerged{1,indEO}.Rlog10filtskip(:,:,:),1))); title('log10(R)')

subplot(5,2,7); imagesc(squeeze(nanmean(mseMerged{1,indEO}.MSEhp(:,:,:),1))); title('Filtskip: HP');
subplot(5,2,8); imagesc(squeeze(nanmean(mseMerged{1,indEO}.Rlog10hp(:,:,:),1))); title('log10(R)')

subplot(5,2,9); imagesc(squeeze(nanmean(mseMerged{1,indEO}.MSEbp(:,:,:),1))); title('Filtskip: BP');
subplot(5,2,10); imagesc(squeeze(nanmean(mseMerged{1,indEO}.Rlog10bp(:,:,:),1))); title('log10(R)')

figure;
subplot(5,2,1); imagesc(squeeze(nanmean(mseMerged{2,indEO}.MSEVanilla(:,:,:),1))); title('Vanilla');
subplot(5,2,2); imagesc(squeeze(nanmean(mseMerged{2,indEO}.Rlog10Vanilla(:,:,:),1))); title('log10(R)')

subplot(5,2,3); imagesc(squeeze(nanmean(mseMerged{2,indEO}.MSEPointavg(:,:,:),1))); title('Pointavg');
subplot(5,2,4); imagesc(squeeze(nanmean(mseMerged{2,indEO}.Rlog10Pointavg(:,:,:),1))); title('log10(R)')

subplot(5,2,5); imagesc(squeeze(nanmean(mseMerged{2,indEO}.MSEfiltskip(:,:,:),1))); title('Filtskip: LP');
subplot(5,2,6); imagesc(squeeze(nanmean(mseMerged{2,indEO}.Rlog10filtskip(:,:,:),1))); title('log10(R)')

subplot(5,2,7); imagesc(squeeze(nanmean(mseMerged{2,indEO}.MSEhp(:,:,:),1))); title('Filtskip: HP');
subplot(5,2,8); imagesc(squeeze(nanmean(mseMerged{2,indEO}.Rlog10hp(:,:,:),1))); title('log10(R)')

subplot(5,2,9); imagesc(squeeze(nanmean(mseMerged{2,indEO}.MSEbp(:,:,:),1))); title('Filtskip: BP');
subplot(5,2,10); imagesc(squeeze(nanmean(mseMerged{2,indEO}.Rlog10bp(:,:,:),1))); title('log10(R)')

%% condition difference

figure;
indEC = 1; indEO = 2; indYA = 1;
subplot(5,2,1); imagesc(squeeze(nanmean(mseMerged{indYA,indEO}.MSEVanilla ,1))-squeeze(nanmean(mseMerged{indYA,indEC}.MSEVanilla ,1))); title('EO vs EC: Vanilla');
subplot(5,2,2); imagesc(squeeze(nanmean(mseMerged{indYA,indEO}.Rlog10Vanilla ,1))-squeeze(nanmean(mseMerged{indYA,indEC}.Rlog10Vanilla ,1))); title('EO vs EC: log10(R)')

subplot(5,2,3); imagesc(squeeze(nanmean(mseMerged{indYA,indEO}.MSEPointavg ,1))-squeeze(nanmean(mseMerged{indYA,indEC}.MSEPointavg ,1))); title('EO vs EC: Pointavg');
subplot(5,2,4); imagesc(squeeze(nanmean(mseMerged{indYA,indEO}.Rlog10Pointavg ,1))-squeeze(nanmean(mseMerged{indYA,indEC}.Rlog10Pointavg ,1))); title('EO vs EC: log10(R)')

subplot(5,2,5); imagesc(squeeze(nanmean(mseMerged{indYA,indEO}.MSEfiltskip ,1))-squeeze(nanmean(mseMerged{indYA,indEC}.MSEfiltskip ,1))); title('EO vs EC: Filtskip: LP');
subplot(5,2,6); imagesc(squeeze(nanmean(mseMerged{indYA,indEO}.Rlog10filtskip ,1))-squeeze(nanmean(mseMerged{indYA,indEC}.Rlog10filtskip ,1))); title('EO vs EC: log10(R)')

subplot(5,2,7); imagesc(squeeze(nanmean(mseMerged{indYA,indEO}.MSEhp ,1))-squeeze(nanmean(mseMerged{indYA,indEC}.MSEhp ,1))); title('EO vs EC: Filtskip: HP');
subplot(5,2,8); imagesc(squeeze(nanmean(mseMerged{indYA,indEO}.Rlog10hp ,1))-squeeze(nanmean(mseMerged{indYA,indEC}.Rlog10hp ,1))); title('EO vs EC: log10(R)')

subplot(5,2,9); imagesc(squeeze(nanmean(mseMerged{indYA,indEO}.MSEbp ,1))-squeeze(nanmean(mseMerged{indYA,indEC}.MSEbp ,1))); title('EO vs EC: Filtskip: BP');
subplot(5,2,10); imagesc(squeeze(nanmean(mseMerged{indYA,indEO}.Rlog10bp ,1))-squeeze(nanmean(mseMerged{indYA,indEC}.Rlog10bp ,1))); title('EO vs EC: log10(R)')

%% age difference: EC

h = figure('units','normalized','position',[.1 .1 .7 .9]);
indEC = 1; indEO = 2; indYA = 1;
subplot(5,2,1); 
    imagesc(squeeze(nanmedian(mseMerged{2,indEC}.MSEVanilla ,1))-squeeze(nanmedian(mseMerged{1,indEC}.MSEVanilla ,1)), [-.1 .1]); title('OA vs YA EC: Vanilla');
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEC}.freq(1:3:41))); xlabel('Scale [Hz]'); ylabel('Channel'); colorbar;
subplot(5,2,2); 
    imagesc(squeeze(nanmedian(mseMerged{2,indEC}.Rlog10Vanilla ,1))-squeeze(nanmedian(mseMerged{1,indEC}.Rlog10Vanilla ,1)), [-.1 .1]); title('OA vs YA EC: log10(R)')
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEC}.freq(1:3:41))); xlabel('Scale [Hz]'); ylabel('Channel'); colorbar;

subplot(5,2,3); 
    imagesc(squeeze(nanmedian(mseMerged{2,indEC}.MSEPointavg ,1))-squeeze(nanmedian(mseMerged{1,indEC}.MSEPointavg ,1)), [-.1 .1]); title('OA vs YA EC: Pointavg');
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEC}.freq(1:3:41))); xlabel('Scale [Hz]'); ylabel('Channel'); colorbar;
subplot(5,2,4); 
    imagesc(squeeze(nanmedian(mseMerged{2,indEC}.Rlog10Pointavg ,1))-squeeze(nanmedian(mseMerged{1,indEC}.Rlog10Pointavg ,1)), [-.1 .1]); title('OA vs YA EC: log10(R)')
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEC}.freq(1:3:41))); xlabel('Scale [Hz]'); ylabel('Channel'); colorbar;
subplot(5,2,5); 
    imagesc(squeeze(nanmedian(mseMerged{2,indEC}.MSEfiltskip ,1))-squeeze(nanmedian(mseMerged{1,indEC}.MSEfiltskip ,1)), [-.1 .1]); title('OA vs YA EC: Filtskip: LP');
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEC}.freq(1:3:41))); xlabel('Scale [Hz]'); ylabel('Channel'); colorbar;
subplot(5,2,6); 
    imagesc(squeeze(nanmedian(mseMerged{2,indEC}.Rlog10filtskip ,1))-squeeze(nanmedian(mseMerged{1,indEC}.Rlog10filtskip ,1)), [-.1 .1]); title('OA vs YA EC: log10(R)')
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEC}.freq(1:3:41))); xlabel('Scale [Hz]'); ylabel('Channel'); colorbar;

subplot(5,2,7); 
    imagesc(squeeze(nanmedian(mseMerged{2,indEC}.MSEhp ,1))-squeeze(nanmedian(mseMerged{1,indEC}.MSEhp ,1)), [-.1 .1]); title('OA vs YA EC: Filtskip: HP');
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEC}.freq(1:3:41))); xlabel('Scale [Hz]'); ylabel('Channel'); colorbar;
subplot(5,2,8); 
    imagesc(squeeze(nanmedian(mseMerged{2,indEC}.Rlog10hp ,1))-squeeze(nanmedian(mseMerged{1,indEC}.Rlog10hp ,1)), [-.1 .1]); title('OA vs YA EC: log10(R)')
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEC}.freq(1:3:41))); xlabel('Scale [Hz]'); ylabel('Channel'); colorbar;

subplot(5,2,9);
    imagesc(squeeze(nanmedian(mseMerged{2,indEC}.MSEbp ,1))-squeeze(nanmedian(mseMerged{1,indEC}.MSEbp,1)), [-.1 .1]); title('OA vs YA EC: Filtskip: BP');
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEC}.freq(1:3:41))); xlabel('Scale [Hz]'); ylabel('Channel'); colorbar;
subplot(5,2,10);
    imagesc(squeeze(nanmedian(mseMerged{2,indEC}.Rlog10bp ,1))-squeeze(nanmedian(mseMerged{1,indEC}.Rlog10bp,1)), [-.1 .1]); title('OA vs YA EC: log10(R)')
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEC}.freq(1:3:41))); xlabel('Scale [Hz]'); ylabel('Channel'); colorbar;

pn.plotFolder = '/Users/kosciessa/Desktop/mntTardis/STSWD_Rest/WIP_eeg/A_MSE_CSD_multiVariant/C_figures/';
figureName = 'E_AgeContrastEC';
% 
% saveas(h, [pn.plotFolder, figureName], 'fig');
% saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

%% plot average scale-wise MSE across channels for different filter settings

h = figure('units','normalized','position',[.1 .1 .7 .4]);
subplot(1,2,1); hold on;
    plot(squeeze(nanmean(squeeze(nanmedian(mseMerged{2,indEO}.MSEVanilla ,1))-squeeze(nanmedian(mseMerged{1,indEO}.MSEVanilla ,1)),1)), 'LineWidth', 2)
    plot(squeeze(nanmean(squeeze(nanmedian(mseMerged{2,indEO}.MSEPointavg ,1))-squeeze(nanmedian(mseMerged{1,indEO}.MSEPointavg ,1)),1)), 'LineWidth', 2)
    plot(squeeze(nanmean(squeeze(nanmedian(mseMerged{2,indEO}.MSEfiltskip ,1))-squeeze(nanmedian(mseMerged{1,indEO}.MSEfiltskip ,1)),1)), 'LineWidth', 2)
    plot(squeeze(nanmean(squeeze(nanmedian(mseMerged{2,indEO}.MSEhp ,1))-squeeze(nanmedian(mseMerged{1,indEO}.MSEhp ,1)),1)), 'LineWidth', 2)
    plot(squeeze(nanmean(squeeze(nanmedian(mseMerged{2,indEO}.MSEbp ,1))-squeeze(nanmedian(mseMerged{1,indEO}.MSEbp ,1)),1)), 'LineWidth', 2)
    legend({'Vanilla'; 'pointavg'; 'filtskip'; 'hp'; 'bp'}); legend('boxoff')
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEO}.freq(1:3:41))); xlabel('Scale [Hz]');
subplot(1,2,2); hold on;
    plot(squeeze(nanmean(squeeze(nanmedian(mseMerged{2,indEO}.Rlog10Vanilla ,1))-squeeze(nanmedian(mseMerged{1,indEO}.Rlog10Vanilla ,1)),1)), 'LineWidth', 2)
    plot(squeeze(nanmean(squeeze(nanmedian(mseMerged{2,indEO}.Rlog10Pointavg ,1))-squeeze(nanmedian(mseMerged{1,indEO}.Rlog10Pointavg ,1)),1)), 'LineWidth', 2)
    plot(squeeze(nanmean(squeeze(nanmedian(mseMerged{2,indEO}.Rlog10filtskip ,1))-squeeze(nanmedian(mseMerged{1,indEO}.Rlog10filtskip ,1)),1)), 'LineWidth', 2)
    plot(squeeze(nanmean(squeeze(nanmedian(mseMerged{2,indEO}.Rlog10hp ,1))-squeeze(nanmedian(mseMerged{1,indEO}.Rlog10hp ,1)),1)), 'LineWidth', 2)
    plot(squeeze(nanmean(squeeze(nanmedian(mseMerged{2,indEO}.Rlog10bp ,1))-squeeze(nanmedian(mseMerged{1,indEO}.Rlog10bp ,1)),1)), 'LineWidth', 2)
    legend({'Vanilla'; 'pointavg'; 'filtskip'; 'hp'; 'bp'}); legend('boxoff')
    set(gca, 'XTick', 1:3:41); set(gca, 'XTickLabel', round(mseMerged{2,indEO}.freq(1:3:41))); xlabel('Scale [Hz]');
set(findall(gcf,'-property','FontSize'),'FontSize',18)
