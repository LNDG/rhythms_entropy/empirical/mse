%% Plot overview of age differences

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';
    load([pn.root, 'B_data/E_mseMerged.mat'], 'mseMerged', 'IDs')

    addpath([pn.root, 'T_tools/fieldtrip-20170904']); ft_defaults;

    load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat'], 'stat', 'methodLabels')

%% FIGURE 5: combination plot of mean traces + stats

    pn.shadedError = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/shadedErrorBar']; addpath(pn.shadedError);

    indCond = 2; channels = 1:60;

    h = figure('units','normalized','position',[0 0 .7 .7]);
    set(0,'DefaultAxesColor',[1 1 1])
    subplot(4,5,1); hold on;
        curAverage = nanmean(mseMerged{1,indCond}.MSEVanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseMerged{2,indCond}.MSEVanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); ylim([0 1.3]); title({'Original'; ''});
        ylim([0.35 1.05]);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
        legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}); legend('boxoff');
    subplot(4,5,2); hold on;
        curAverage = nanmean(mseMerged{1,indCond}.MSEPointavg(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseMerged{2,indCond}.MSEPointavg(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); ylim([0 1.3]); title({'Original + varying bounds'; ''});
        ylim([0.35 1.05]);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
    subplot(4,5,3); hold on;
        curAverage = nanmean(mseMerged{1,indCond}.MSEfiltskip(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseMerged{2,indCond}.MSEfiltskip(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); ylim([0 1.3]); title({'Low-pass'; ''});
        ylim([0.35 1.05]);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
    subplot(4,5,4); hold on;
        curAverage = nanmean(mseMerged{1,indCond}.MSEhp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseMerged{2,indCond}.MSEhp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); ylim([0 1.3]); title({'High-pass'; ''});
        ylim([0.3 0.7]);
        freqContent = (1./[2:43])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
    subplot(4,5,5); hold on;
        curAverage = nanmean(mseMerged{1,indCond}.MSEbp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseMerged{2,indCond}.MSEbp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); ylim([0 1.3]); title({'Band-pass'; ''});
        ylim([0.2 0.65]);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Sample Entropy');
    subplot(4,5,2*5+1); hold on;
        curAverage = nanmean(mseMerged{1,indCond}.Rvanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseMerged{2,indCond}.Rvanilla(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); ylim([0 9]*10^-4); %title({'Vanilla (r = .5, m = 2)'; ''});
        ylim([7.5 9]*10^-4);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Similarity Bounds');
        %legend([l1.mainLine, l2.mainLine],{'Young adults'; 'Older adults'}); legend('boxoff');
    subplot(4,5,2*5+2); hold on;
        curAverage = nanmean(mseMerged{1,indCond}.Rpointavg(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseMerged{2,indCond}.Rpointavg(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); ylim([0 9]*10^-4); %title({'Vanilla + scale-wise R'; ''});
        ylim([4 9]*10^-4);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Similarity Bounds');
    subplot(4,5,2*5+3); hold on;
        curAverage = nanmean(mseMerged{1,indCond}.Rfiltskip(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseMerged{2,indCond}.Rfiltskip(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); ylim([0 9]*10^-4); %title({'Lowpass + scale-wise R'; ''});
        ylim([4 9]*10^-4);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Similarity Bounds');
    subplot(4,5,2*5+4); hold on;
        curAverage = nanmean(mseMerged{1,indCond}.Rhp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseMerged{2,indCond}.Rhp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); ylim([0 9]*10^-4); %title({'Highpass + scale-wise R'; ''});
        ylim([0 8]*10^-4);
        freqContent = (1./[2:43])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Similarity Bounds');
    subplot(4,5,2*5+5); hold on;
        curAverage = nanmean(mseMerged{1,indCond}.Rbp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l1 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'r','linewidth', 1.5}, 'patchSaturation', .05);
        curAverage = nanmean(mseMerged{2,indCond}.Rbp(:, channels,:),2);
        standError = nanstd(curAverage,1)./sqrt(size(curAverage,1));
        l2 = shadedErrorBar([],nanmean(curAverage,1),standError, 'lineprops', {'k','linewidth', 1.5}, 'patchSaturation', .05);
        xlim([1 41]); ylim([0 9]*10^-4); %title({'Bandpass + scale-wise R'; ''});
        ylim([0 2.5]*10^-4);
        freqContent = (1./[1:42])*250;
        set(gca, 'XTick', 1:8:41); set(gca, 'XTickLabel', round(freqContent(1:8:41))); xlabel('Scale [Hz]'); ylabel('Similarity Bounds');
    set(findall(gcf,'-property','FontSize'),'FontSize',18)
    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'FX_MSE_R_byMethod';

%% plot EO: OA vs. YA MSE CBPA

    indContrast = 4;
    labels = {'MSE: Vanilla'; 'MSE: Vanilla + scale-wise R'; 'MSE: Lowpass + scale-wise R'; 'MSE: Highpass + scale-wise R'; 'MSE: Bandpass + scale-wise R';...
        'r: Vanilla'; 'r: Vanilla + scale-wise R'; 'r: Lowpass + scale-wise R'; 'r: Highpass + scale-wise R'; 'r: Bandpass + scale-wise R'};

    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/altmany-export_fig-4282d74')

    %h = figure('units','normalized','position',[0 0 1 .4]);
    for indMethod = 1:10
        if indMethod>5
            subplot(4,5,2*5+indMethod);
        else
            subplot(4,5,5+indMethod);
        end
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', .4); 
        caxis([-6 6]) 
        hold on;
        imagesc(stat{indContrast,indMethod}.stat, 'AlphaData', stat{indContrast,indMethod}.mask); 
        set(gca, 'XTick', [1:8:35, 42]);
        if ismember(indMethod, [4, 9])
            freqContent = fliplr((1./[2:43])*250);
        else
            freqContent = fliplr((1./[1:42])*250);
        end
        set(gca, 'XTickLabels', round(freqContent(get(gca, 'XTick')),0));
        set(gca, 'XDir', 'reverse')
        set(gca, 'YTick', []);
        xlabel('Scale (Hz)'); ylabel({'Channels'; 'ant.-posterior'});
        %cb = colorbar('location', 'SouthOutside'); set(get(cb,'label'),'string','t values');
        %title(labels(indMethod))
    end
    cb = colorbar('location', 'EastOutside'); set(get(cb,'label'),'string','t values');
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/T_tools/brewermap')
    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);
    colormap(cBrew)

    set(findall(gcf,'-property','FontSize'),'FontSize',20)

    pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/C_figures/';
    figureName = 'Z2_StatsOverview_integrated';
    print(h, '-dpdf', [pn.plotFolder, figureName, '.pdf'])
    %export_fig([pn.plotFolder, figureName, '.eps'], '-transparent')
    saveas(h, [pn.plotFolder, figureName], 'fig');
    saveas(h, [pn.plotFolder, figureName], 'png');
