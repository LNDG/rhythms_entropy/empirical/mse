%% create overview figure of entropy, power and PSD slope age differences

    pn.root = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/A_MSE_CSD_multiVariant/';

    addpath([pn.root, 'T_tools/brewermap']) % add colorbrewer

    cBrew = brewermap(500,'RdBu');
    cBrew = flipud(cBrew);

    addpath([pn.root, 'T_tools/']) % add convertPtoExponential

%% FIGURE 4B-D: plot topographies of MSE age differences

    addpath([pn.root, 'T_tools/fieldtrip-20170904']); ft_defaults;
    load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat'], 'stat', 'methodLabels')
    contrastLabels = {'YA: EC vs. EO'; 'OA: EC vs. EO'; 'EC: OA vs. YA'; 'EO: OA vs. YA'};

    cfg = [];
    cfg.layout = 'acticap-64ch-standard2.mat';
    cfg.parameter = 'powspctrm';
    cfg.comment = 'no';
    cfg.colorbar = 'EastOutside';
    cfg.style = 'both';
    cfg.colormap = cBrew;

%% plot topography of coarse-scale entropy difference

    % Note that the internal FieldTrip cluster statistics reorder the entropy
    % timescales according to increasing frequency.

    h = figure('units','normalized','position',[.1 .1 .7 .45]);
    subplot(2,3,1)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,1}.mask(:,1:5),[],2));
        cfg.highlightcolor = [0 0 0];
        cfg.zlim = [-5 5];
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,1:5),2));
        ft_topoplotER(cfg,plotData);
        cb = colorbar; set(get(cb,'title'),'string','t values');
        pval = []; pval = convertPtoExponential(stat{4,1}.negclusters(1).prob);
        title({'Coarse scale entropy OA minus YA', ['p = ', pval{1}]});
        
%% plot topography of high frequency power difference

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/C_stat.mat')

    subplot(2,3,3+1)
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        cfg.zlim = [-3 3];
        idxFreqs = stat{4,1}.freq >= 32 & stat{4,1}.freq <= 64;
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{4,1}.label(max(stat{4,1}.mask(:,idxFreqs),[],2));
        cfg.highlightcolor = [0 0 0];
        plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,idxFreqs),2));
        ft_topoplotER(cfg,plotData);
        cb = colorbar; set(get(cb,'title'),'string','t values');
        pval = []; pval = convertPtoExponential(stat{4,1}.posclusters(1).prob);
        title({'32-64 Hz Power OA minus YA', ['p = ', pval{1}]});

%% plot topography of fine-scale entropy difference

    load([pn.root, 'B_data/E2_CBPA_Age_Condition_v2.mat'], 'stat', 'methodLabels')
    
    subplot(2,3,2)
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{1,1}.label(max(stat{4,1}.mask(:,41),[],2));
        cfg.highlightcolor = [0 0 0];
        cfg.highlightsymbol = '*';
        cfg.zlim = [-5 5];
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,41),2));
        ft_topoplotER(cfg,plotData);
        cb = colorbar; set(get(cb,'title'),'string','t values');
        pval = []; pval = convertPtoExponential(stat{4,1}.posclusters(1).prob);
        title({'Fine scale entropy OA minus YA', ['p = ', pval{1}]});
        
%% plot topography of low frequency power age differences

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/C_stat.mat')

    subplot(2,3,3+2)
        plotData = [];
        plotData.label = stat{1,1}.label; % {1 x N}
        plotData.dimord = 'chan';
        cfg.zlim = [-3 3];
        idxFreqs = stat{4,1}.freq >= 8 & stat{4,1}.freq <= 15;
        cfg.highlight = 'on';
        cfg.highlightchannel = stat{4,1}.label(max(stat{4,1}.mask(:,idxFreqs),[],2));  
        cfg.highlightcolor = [0 0 0];
        plotData.powspctrm = squeeze(nanmean(stat{4,1}.stat(:,idxFreqs),2));
        ft_topoplotER(cfg,plotData);
        cb = colorbar; set(get(cb,'title'),'string','t values');
        pval = []; pval = convertPtoExponential(stat{4,1}.negclusters(1).prob);
        title({'8-15 Hz Power OA minus YA', ['p = ', pval{1}]});

%% plot topographies of PSD slope differences form zero (i.e., part 1)

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes.mat')
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D2_CBPASlopeAge.mat')

    subplot(2,3,3);
        cfg.highlight = 'yes';
        cfg.highlightchannel =  grandavg_EC.label(stat_zero.mask);
        cfg.highlightcolor = [0 0 0];
        cfg.zlim = [-.03 .03];
        plotData = [];
        plotData.label = grandavg_EC.label; % {1 x N}
        plotData.dimord = 'chan';
        plotData.powspctrm = squeeze(nanmean(linFit_2_30_EO,1))';
        ft_topoplotER(cfg,plotData);
        cb = colorbar; set(get(cb,'title'),'string','slope');
        pval = []; pval = convertPtoExponential(stat_zero.negclusters.prob);
        title({'Spectral slopes across age minus 0', ['p = ', pval{1}]});
        
%% plot topographies of PSD slope age differences (i.e., part 2)

    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D_PSDslopes.mat')
    load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/rest/B_analyses/B_FFT/B_data/D2_CBPASlopeAge.mat')

    subplot(2,3,6);
        cfg.zlim = [-7 7];
        cfg.highlight = 'yes';
        cfg.highlightchannel =  grandavg_EC.label(stat.mask);
        cfg.highlightcolor = [0 0 0];
        plotData.powspctrm = stat.stat;
        ft_topoplotER(cfg,plotData);
        cb = colorbar; set(get(cb,'title'),'string','t values');
        pval = []; pval = convertPtoExponential(stat.posclusters.prob);
        title({'Spectral slopes: OA minus YA', ['p = ', pval{1}]});

    set(findall(gcf,'-property','FontSize'),'FontSize',17)

%% move subplots closer together

    AxesHandle=findobj(h,'Type','axes');
    axisPos = [];
    leftShift = -.1; upShift = +.08;
    for indAx = 1:6
        axInd = 6-indAx+1;
        axisPos{indAx} = get(AxesHandle(axInd),'OuterPosition');
        if mod(indAx,2)==1
            % shift only left
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2) axisPos{indAx}(3) axisPos{indAx}(4)]);
        elseif mod(indAx,2)==0
            % shift left and up
            set(AxesHandle(axInd),'OuterPosition',[axisPos{indAx}(1)+leftShift axisPos{indAx}(2)+upShift axisPos{indAx}(3) axisPos{indAx}(4)]);
            % increase leftwards shift for next element
            leftShift = leftShift-.09;
        end
    end
    
%% save figure

    pn.plotFolder = [pn.root, 'C_figures/'];
    figureName = 'G_FigureTopographySummary';
    saveas(h, [pn.plotFolder, figureName], 'epsc');
    saveas(h, [pn.plotFolder, figureName], 'png');